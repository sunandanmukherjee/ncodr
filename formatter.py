import pandas as pd

def format_features(fname, mode):
    seq_prop = pd.read_csv(fname + ".tsv", sep='\t')
    seq_prop = seq_prop.loc[:, ~seq_prop.columns.str.contains('^Unnamed')]
    if mode != 4:
        features = pd.read_csv("windowsize3.tsv", sep='\t')
        features = features.loc[:, ~features.columns.str.contains('^Unnamed')]
    if mode != 5:
        genstat_data = pd.read_csv(fname+".genstats.tsv", sep='\t')
        genstat_data = genstat_data.loc[:, ~genstat_data.columns.str.contains('^Unnamed')]

    if mode == 0:
        temp1 = pd.merge(seq_prop, features, on='ID')
        temp2 = pd.merge(temp1, genstat_data, on='ID')
        columns = ["ID", "Percent AU", "MFEI", "Sequence Length", "AAA", "AAU", "AAC", "AAG",
                    "AUA", "AUU", "AUC", "AUG", "ACA", "ACU", "ACC", "ACG", "AGA", "AGU",
                    "AGC", "AGG", "UAA", "UAU", "UAC", "UAG", "UUA", "UUU", "UUC", "UUG",
                    "UCA", "UCU", "UCC", "UCG", "UGA", "UGU", "UGC", "UGG", "CAA", "CAU",
                    "CAC", "CAG", "CUA", "CUU", "CUC", "CUG", "CCA", "CCU", "CCC", "CCG",
                    "CGA", "CGU", "CGC", "CGG", "GAA", "GAU", "GAC", "GAG", "GUA", "GUU",
                    "GUC", "GUG", "GCA", "GCU", "GCC", "GCG", "GGA", "GGU", "GGC", "GGG",
                    "Npb", "NQ", "ND"]
        temp3 = temp2[columns]
        temp3.to_csv(fname+'.data', header=False, index=False, sep ='\t')

    if mode == 1:
        temp1 = pd.merge(seq_prop, features, on='ID')
        temp2 = pd.merge(temp1, genstat_data, on='ID')
        columns = ["ID", "Percent AU", "MFEI", "AAA", "AAU", "AAC", "AAG",
                    "AUA", "AUU", "AUC", "AUG", "ACA", "ACU", "ACC", "ACG", "AGA", "AGU",
                    "AGC", "AGG", "UAA", "UAU", "UAC", "UAG", "UUA", "UUU", "UUC", "UUG",
                    "UCA", "UCU", "UCC", "UCG", "UGA", "UGU", "UGC", "UGG", "CAA", "CAU",
                    "CAC", "CAG", "CUA", "CUU", "CUC", "CUG", "CCA", "CCU", "CCC", "CCG",
                    "CGA", "CGU", "CGC", "CGG", "GAA", "GAU", "GAC", "GAG", "GUA", "GUU",
                    "GUC", "GUG", "GCA", "GCU", "GCC", "GCG", "GGA", "GGU", "GGC", "GGG",
                    "Npb", "NQ", "ND"]
        temp3 = temp2[columns]
        temp3.to_csv(fname+'.data', header=False, index=False, sep ='\t')

    if mode == 2:
        temp1 = pd.merge(seq_prop, features, on='ID')
        temp2 = pd.merge(temp1, genstat_data, on='ID')
        columns = ["ID", "MFEI", "Sequence Length", "AAA", "AAU", "AAC", "AAG",
                    "AUA", "AUU", "AUC", "AUG", "ACA", "ACU", "ACC", "ACG", "AGA", "AGU",
                    "AGC", "AGG", "UAA", "UAU", "UAC", "UAG", "UUA", "UUU", "UUC", "UUG",
                    "UCA", "UCU", "UCC", "UCG", "UGA", "UGU", "UGC", "UGG", "CAA", "CAU",
                    "CAC", "CAG", "CUA", "CUU", "CUC", "CUG", "CCA", "CCU", "CCC", "CCG",
                    "CGA", "CGU", "CGC", "CGG", "GAA", "GAU", "GAC", "GAG", "GUA", "GUU",
                    "GUC", "GUG", "GCA", "GCU", "GCC", "GCG", "GGA", "GGU", "GGC", "GGG",
                    "Npb", "NQ", "ND"]
        temp3 = temp2[columns]
        temp3.to_csv(fname+'.data', header=False, index=False, sep ='\t')

    if mode == 3:
        temp1 = pd.merge(seq_prop, features, on='ID')
        temp2 = pd.merge(temp1, genstat_data, on='ID')
        columns = ["ID", "Percent AU", "Sequence Length", "AAA", "AAU", "AAC", "AAG",
                    "AUA", "AUU", "AUC", "AUG", "ACA", "ACU", "ACC", "ACG", "AGA", "AGU",
                    "AGC", "AGG", "UAA", "UAU", "UAC", "UAG", "UUA", "UUU", "UUC", "UUG",
                    "UCA", "UCU", "UCC", "UCG", "UGA", "UGU", "UGC", "UGG", "CAA", "CAU",
                    "CAC", "CAG", "CUA", "CUU", "CUC", "CUG", "CCA", "CCU", "CCC", "CCG",
                    "CGA", "CGU", "CGC", "CGG", "GAA", "GAU", "GAC", "GAG", "GUA", "GUU",
                    "GUC", "GUG", "GCA", "GCU", "GCC", "GCG", "GGA", "GGU", "GGC", "GGG",
                    "Npb", "NQ", "ND"]
        temp3 = temp2[columns]
        temp3.to_csv(fname+'.data', header=False, index=False, sep ='\t')

    if mode == 4:
        temp2 = pd.merge(seq_prop, genstat_data, on='ID')
        columns = ["ID", "Percent AU", "MFEI", "Sequence Length", "Npb", "NQ", "ND"]
        temp3 = temp2[columns]
        temp3.to_csv(fname+'.data', header=False, index=False, sep ='\t')

    if mode == 5:
        temp2 = pd.merge(seq_prop, features, on='ID')
        columns = ["ID", "Percent AU", "MFEI", "Sequence Length", "AAA", "AAU", "AAC", "AAG",
                    "AUA", "AUU", "AUC", "AUG", "ACA", "ACU", "ACC", "ACG", "AGA", "AGU",
                    "AGC", "AGG", "UAA", "UAU", "UAC", "UAG", "UUA", "UUU", "UUC", "UUG",
                    "UCA", "UCU", "UCC", "UCG", "UGA", "UGU", "UGC", "UGG", "CAA", "CAU",
                    "CAC", "CAG", "CUA", "CUU", "CUC", "CUG", "CCA", "CCU", "CCC", "CCG",
                    "CGA", "CGU", "CGC", "CGG", "GAA", "GAU", "GAC", "GAG", "GUA", "GUU",
                    "GUC", "GUG", "GCA", "GCU", "GCC", "GCG", "GGA", "GGU", "GGC", "GGG",]
        temp3 = temp2[columns]
        temp3.to_csv(fname+'.data', header=False, index=False, sep ='\t')
    temp3.ID.to_csv(fname+'.ids', header=False, index=False)
